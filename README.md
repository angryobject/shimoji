Inpired by this [tweet](https://twitter.com/ftrain/status/360833985187807233). A simple scripts to randomly choose a emoji from a unicode range or from a list (or both). Works with zsh. May work with bash or other shells.

In your `.zshrc`:

```
PROMPT='$(path/to/shimoji) > '
```